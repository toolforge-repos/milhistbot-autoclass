using System;
using System.Collections.Generic;
using NDesk.Options;
using Wikimedia;
using Wikimedia.MilHist;

namespace AutoClass
{
    public class AutoClass
    {
        private int max = 1;
        private bool debugs = false;
        private bool force = false;
        private bool help = false;
        private bool verbose = false;
        private readonly List<string> articles;

        private void WriteLine(string line)
        {
            if (debugs || verbose)
            {
                Console.WriteLine(line);
            }
        }

        private void Process(ArticlePage article, TalkPage talk)
        {
            try
            {
                Console.WriteLine("Process:" + article.Title);

                if (article.IsRedirect)
                {
                    var redirect = article.RedirectsTo as ArticlePage;
                    if (null == redirect)
                    {
                        throw new Exception("Bad redirect: " + article.Title);
                    }
                    else
                    {
                        article = redirect;
                        WriteLine("    Redirects to " + article.Title);
                    }
                }

                var taskForces = new TaskForces(article);
                if (taskForces.Any())
                {
                    talk.Load();
                    talk.MilHist.ProjectTemplate.Add(taskForces);
                    WriteLine("   Added " + taskForces);
                    if (force)
                    {
                       talk.Save("AutoClass: Added task forces");
                    }
                }
                else
                {
                    Console.Error.WriteLine(article.Title + ": No task forces found");
                }
            }
            catch (PageMissingException pmex)
            {
                Cred.Instance.Warning(pmex.Message);
            }
        }

        private void Process(FilePage article)
        {
            try
            {
                Console.WriteLine("FilePage Process:" + article.Title);

                var taskForces = new TaskForces(article);
                if (taskForces.Any())
                {
                    FileTalkPage talk = article.Talk;
                    talk.Load();
                    talk.MilHist.ProjectTemplate.Add(taskForces);
                    Console.WriteLine("   Added " + taskForces);
                    if (force)
                    { 
                      talk.Save("AutoClass: Added task forces");
                    }
                }
                else
                {
                    Console.Error.WriteLine(article.Title + ": No task forces found");
                }
            }
            catch (PageMissingException pmex)
            {
                Cred.Instance.Warning(pmex.Message);
            }
        }


        private void Process()
        {
            Cred.Instance.Showtime("started");
            if (articles.Any())
            {
                 var article = new ArticlePage (articles.First());
                 Process(article, article.Talk);
            }
            else
            {
                var query = new Query("Military history articles with no associated task force", max);
                var pages = query.Pages;
                foreach (var page in pages)
                {
                    Console.WriteLine(page.Title);
                    if (page is TalkPage)
                    {
                        var talkPage = page as TalkPage;
                        if (null != talkPage)
                        {
                            // in the case of a redirect, talkPage may not be article.Talk! 
                            Process(talkPage.Article, talkPage);
                        }
                    }
                    if (page is FileTalkPage)
                    {
                        var fileTalkPage = page as FileTalkPage;
                        if (null != fileTalkPage)
                        {
                            Process(fileTalkPage.Article);
                        }
                    }
                }
            }
            Cred.Instance.Showtime("done");
        }

        static int ParseIntOrDie(string someStr)
        {
            if (int.TryParse(someStr, out int result))
            {
                return result;
            }
            throw new Exception("Could not convert '" + someStr + "' to an integer");
        }

        private static void ShowHelp(OptionSet optionSet)
        {
            Console.WriteLine("Usage: mono AutoClass [OPTIONS]+ <article>");
            Console.WriteLine("Add task forces to the MilHist template on the talk page.");
            Console.WriteLine();
            Console.WriteLine("Options:");
            optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }

        List<string> Options(string[] args)
        {
            var optionSet = new OptionSet() 
            {
                { "d|debug",   "debugging",    v => debugs  = v != null },
                { "f|force",   "update page",  v => force   = v != null },
                { "h|?|help",  "display help", v => help    = v != null },
                { "n|max=",    "number of pages to process",  v => max = ParseIntOrDie(v) },
                { "v|verbose", "vebosity",     v => verbose = v != null },
            };

            List<string> extras = optionSet.Parse(args);

            if (help)
            {
                ShowHelp(optionSet);
            }

            return extras;
        }

        private AutoClass(string[] args)
        {
            articles  = Options(args);
            Debug.On  = debugs;
        }

        static public void Main(string[] args)
        {
            var autoClass = new AutoClass(args);
            autoClass.Process();
        }
    }
}